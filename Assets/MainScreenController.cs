﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

public class MainScreenController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
     //.RequestServerAuthCode(false /* Don't force refresh */)
     .Build();

        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();

        if (PlayerPrefs.HasKey("isloggedIn"))
        {
            PlayerPrefs.DeleteKey("isloggedIn");
        }
        if (!PlayerPrefs.HasKey("Score"))
        {
            PlayerPrefs.SetInt("Score", 0);
        }
        ScoringSystem.ResetRankingData();
    }

   
}
