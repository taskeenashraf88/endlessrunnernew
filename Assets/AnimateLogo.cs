﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AnimateLogo : MonoBehaviour
{
    Image image;
    public float fadeSpeed = 0.7f;
    Color tempColor;
    bool startAnimating;
    bool increasOpacity;
    //bool allAnimationsplayed;

    public Sprite nextLogo;
    // Start is called before the first frame update
    void Start()
    {
        startAnimating = true;
        increasOpacity = true;
        //allAnimationsplayed = false;
        image = GetComponent<Image>();
        tempColor = image.color;
        tempColor.a = 0f;
        image.color = tempColor;
        StartCoroutine(LoadNextScene());
    }

    // Update is called once per frame
    void Update()
    {
        
            if (startAnimating)
            {
                if (increasOpacity)
                {
                    if (tempColor.a < 1.3f)
                    {
                        tempColor.a = tempColor.a + fadeSpeed * Time.deltaTime;
                        image.color = tempColor;
                    }
                    else
                    {
                        increasOpacity = false;

                    }
                }
                else
                {
                    if (tempColor.a > 0)
                    {
                        tempColor.a = tempColor.a - fadeSpeed * Time.deltaTime;
                        image.color = tempColor;
                    }
                    else
                    {
                        startAnimating = false;
                    }

                }
            }
            else
            {
                image.sprite = nextLogo;
                increasOpacity = true;
                startAnimating = true;

            }
        }


    IEnumerator LoadNextScene()
    {
        yield return new WaitForSeconds(7.48f);
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        SceneManager.LoadScene("Lobby Scene");
    }
        
     

}
