﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject ObstaclePrefab;
    public Vector3 spawnPos;
    public float startDelay = 2;
    public string columnPerfabName;
    public string columnPerfabName2;
    public float repeatRate = 5;
    private PlayerMovement playerControllerScript;
    public int spawnItemNumber = 5;
    public float distanceItem = 0.9f;

    void Start()
    {
        //Spawn obstacle at random intervals
        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerMovement>();
      
            if (ObstaclePrefab.name == columnPerfabName || ObstaclePrefab.name == columnPerfabName2)
            {
                InvokeRepeating("SpawnColumns", startDelay, Random.Range(4f, 10.0f));
            }
            else
            {
                InvokeRepeating("SpawnObstacle", startDelay, repeatRate);
            }
        
    }

    
    void Update()
    {
        
    }

    void SpawnColumns()
    {
        if (GameController.instance.isStartRunning)
        {
            if (playerControllerScript.gameOver == false)
            {
                GameObject createdObject = Instantiate(ObstaclePrefab, spawnPos, ObstaclePrefab.transform.rotation);

              
                    createdObject.transform.localScale = new Vector3(Random.Range(0.3f, 1f), Random.Range(0.3f, 0.8f), createdObject.transform.localScale.z);
                
            }
        }
    }

    void SpawnObstacle()
    {
        Vector3 position = spawnPos;
        if (GameController.instance.isStartRunning)
        {
            if (playerControllerScript.gameOver == false)
            {
                for (int i = 0; i < spawnItemNumber; i++)
                {
                    position.z+= distanceItem;
                    //Debug.Log(ObstaclePrefab);
                    Instantiate(ObstaclePrefab,position , ObstaclePrefab.transform.rotation);
                }

               

            }
        }
    }


}
