﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLeft : MonoBehaviour
{

    public float speedMin = 5;
    public float speedMax = 15;
    private float speed=10;
    private PlayerMovement playerControllerScript;
    public float leftBound;
    public float leftSecondBound;
    private GameObject player;
    private bool scoreAdded;
    
    void Start()
    {
        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerMovement>();
        player = GameObject.Find("Player");
        scoreAdded = false;

    }


    void Update()
    {
        if (playerControllerScript.gameOver == false && playerControllerScript.gamestarted)
        {
            if (this.CompareTag("Obstacle"))
            {
                if (this.transform.position.z < player.transform.position.z && !scoreAdded)
                {

                    ScoringSystem.theScore += 5;
                    scoreAdded = true;


                }
                //transform.Translate(Vector3.back * Time.deltaTime * Random.Range(speedMin, speedMax));
                transform.Translate(Vector3.back * Time.deltaTime * speed);

            }
            else
            {
                //transform.Translate(Vector3.up * Time.deltaTime * Random.Range(speedMin, speedMax));
                transform.Translate(Vector3.up * Time.deltaTime * speed);

            }


            if (transform.position.z < leftBound && gameObject.CompareTag("Obstacle"))
            {
                Destroy(gameObject);

            }

            if (transform.position.z < leftSecondBound)
            {
                Destroy(gameObject.transform.parent);

            }

        }

        
    }
}
