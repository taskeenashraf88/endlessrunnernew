﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using CloudOnce;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using System.Threading.Tasks;


public class CloudOnceManager : Singleton<CloudOnceManager>
{
    private string leaderboardId = "CgkI7KLtud4cEAIQAQ";
    string[] IDs;
    public string playerScore = "";
    public bool isInitialized=false;
    public string userName = " ";

    public bool reloadScores;

    public bool reloadLobbyScore;



    //public void Start()
    //{
    //    isLoggedIn = false;
    //}

    public void SetUpCloud()
    {
        reloadScores = false;
        reloadLobbyScore = false;
        playerScore = "";
        if (!PlayerPrefs.HasKey("isloggedIn"))
        {
                PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
     //.RequestServerAuthCode(false /* Don't force refresh */)
     .Build();

                PlayGamesPlatform.InitializeInstance(config);
                PlayGamesPlatform.Activate();
            
       Debug.Log("PlayGamesPlatform.Instance.localUser.authenticated" + PlayGamesPlatform.Instance.localUser.authenticated);

   



       Debug.Log("initiliazed playgames");
            if (!PlayGamesPlatform.Instance.IsAuthenticated())
            {
                PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptOnce, (result) =>
                {
                    if (result == SignInStatus.Success)
                    {
                        GetHighScore();
                        PlayerPrefs.SetInt("isloggedIn", 1);
                        userName = PlayGamesPlatform.Instance.localUser.userName;
                    }
                    else
                    {
                        Debug.Log("failed to login");
                    }

                });
            }
            else
            {
                Debug.Log("player already logged in");


         GetHighScore();

            }
        }
        else
        {
            Debug.Log("play games already initialzed");
            GetHighScore();
        }
    }

   

    public void AddHighScore()
    {
        

        Debug.Log("Debug:Adding score to leaderboard " + PlayerPrefs.GetInt("Score", 0));
        PlayGamesPlatform.Instance.ReportScore(PlayerPrefs.GetInt("Score", 0), leaderboardId, (bool success) => {
            Debug.Log("Result of submit score" + success);
        });
        if (playerScore.Length > 0)
        {
            if (Int32.Parse(playerScore) < PlayerPrefs.GetInt("Score", 0))
            {
                GetHighScore();
                reloadScores = true;
            }
        }

    }

    public void GetHighScore()
    {
        int i = 0;
       


              IDs = new string[10];

        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {

            //Leaderboards.Leaderboard1.LoadScores(scores =>
            //{
            //    if (scores.Length > 0)
            //    {
            //        Debug.Log("Got " + scores.Length + " scores");
            //        string myScores = "Leaderboard:\n";
            //        ScoringSystem.scores = new int[scores.Length];
            //        ScoringSystem.names = new string[scores.Length];
            //        foreach (IScore score in scores)
            //        {
            //            IDs[i] = score.userID;

            //            myScores += "\t" + score.userID + " " + score.formattedValue + " " + score.date + "\n";
            //            ScoringSystem.scores[i] = Int32.Parse(score.formattedValue);
            //            i++;
            //        }
            //        i = 0;
            //        Debug.Log(myScores);
            //        PlayGamesPlatform.Instance.LoadUsers(IDs, (profile) =>
            //        {

            //            foreach (var p in profile)
            //            {
            //                ScoringSystem.names[i] = p.userName;
            //                Debug.Log(p.userName);
            //                Debug.Log(p.id);
            //                i++;

            //            }

            //        });

            //    }
            //    else
            //        Debug.Log("No scores loaded");
            //});

           
                Debug.Log("***Get high score from leaderboard");

                PlayGamesPlatform.Instance.LoadScores(
        leaderboardId,
        LeaderboardStart.TopScores,
        10,
        LeaderboardCollection.Public,
        LeaderboardTimeSpan.AllTime,
        (data) =>
        {
            string mStatus = "Leaderboard data valid: " + data.Valid;
            mStatus += "\n approx:" + data.ApproximateCount + " have " + data.Scores.Length;
            Debug.Log(mStatus);

            playerScore = data.PlayerScore.formattedValue;
            Debug.Log("player score extracted from leaderboard" + playerScore);
            if (Int32.Parse(playerScore) > PlayerPrefs.GetInt("Score", 0))
            {
                PlayerPrefs.SetInt("Score", Int32.Parse(playerScore));
                reloadLobbyScore = true;
            }
            if (data.Scores.Length > 0)
            {
                PlayerPrefs.SetInt("isloggedIn", 1);
                //ScoringSystem.scores = new int[data.Scores.Length];
                //ScoringSystem.names = new string[data.Scores.Length];
                Debug.Log(data.ApproximateCount);
                Debug.Log(data.Scores);
                foreach (var score in data.Scores)
                {
                    IDs[i] = score.userID;
                    ScoringSystem.scores[i] = Int32.Parse(score.formattedValue);
                    Debug.Log(ScoringSystem.scores[i]);
                    Debug.Log(score.userID);
                    i++;
                }
                i = 0;

                PlayGamesPlatform.Instance.LoadUsers(IDs, (profile) =>
                {

                    foreach (var p in profile)
                    {
                        if (userName.Contains(p.userName))
                        {
                            if (Int32.Parse(playerScore) < PlayerPrefs.GetInt("Score", 0))
                            {
                                playerScore = PlayerPrefs.GetInt("Score", 0).ToString();
                                ScoringSystem.scores[i] = PlayerPrefs.GetInt("Score", 0);

                            }
                            Debug.Log(playerScore);
                        }
                        
                        ScoringSystem.names[i] = p.userName;
                        Debug.Log(p.userName);
                        Debug.Log(p.id);
                        i++;

                    }

                });



            }


        });

            



        }

    }
}
