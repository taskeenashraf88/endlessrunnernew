﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{

    private Rigidbody playerRB;
    private Animator playerAnim;
    private AudioSource playerAudio;
    private BoxCollider playerCollider;
  
    public ParticleSystem dirtPart;
    public AudioClip jumpSound;
    public AudioClip crashSound;
    public float jumpForce = 10f;
    public float gravityModifier;
    public bool isOnGround = true;
    public bool gameOver = false;

    public bool gamestarted = false;

    public GameObject ProgressBar;
    private float fillamount = 1f;

    public GameObject scorePanel;

    public Text score;

    void Start()
    {
        playerRB = GetComponent<Rigidbody>();
        playerAnim = GetComponent<Animator>();
        playerCollider = GetComponent<BoxCollider>();
        //Physics.gravity *= gravityModifier;
        playerAudio = GetComponent<AudioSource>();
        if (!gamestarted)
        {
            dirtPart.Stop();
        }
        
    }

    public void GetMovementControls(bool input)
    {
        if (gamestarted && !gameOver)
        {

            if (input)
            {
                if (isOnGround)
                {
                    Debug.Log("space pressed");
                    playerRB.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                    isOnGround = false;
                    playerAnim.SetTrigger("Jump_trig");
                    dirtPart.Stop();
                    playerAudio.PlayOneShot(jumpSound, 1.0f);
                }

            }
            else
            {
                playerAnim.SetTrigger("Slip_trig");
                dirtPart.Stop();
                playerAudio.PlayOneShot(jumpSound, 1.0f);
                StartCoroutine(SetColliderDuringSlideCoroutine());
            }

        }
    }
    void Update()
    {

       if(!gamestarted)
        {

            // Stop particle effect if the player is in idle state
            dirtPart.Stop();

            // make the animation of run to play
            if (GameController.instance.isStartRunning)
            {
                playerAnim.SetBool("Start_run", true);
                gamestarted = true;
                dirtPart.Play();
            }
        }
       if(gameOver)
        {
            dirtPart.Stop();
        }
        
        
    }

    IEnumerator SetColliderDuringSlideCoroutine()
    {
        playerCollider.center = new Vector3(playerCollider.center.x, 0.42f, playerCollider.center.z);
        playerCollider.size = new Vector3(playerCollider.size.x, 0.85f, playerCollider.size.z);
        yield return new WaitForSeconds(0.5f);
        playerCollider.center = new Vector3(playerCollider.center.x, 0.86f, playerCollider.center.z);
        playerCollider.size = new Vector3(playerCollider.size.x, 1.75f, playerCollider.size.z);
    }

    private void OnCollisionEnter(Collision collision)
    {
        
        if (collision.gameObject.CompareTag("Ground"))
        {
        isOnGround = true;
        dirtPart.Play();
        }
        else if (collision.gameObject.CompareTag("Obstacle"))
        {
            GameController.instance.lives--;

            ProgressBar.GetComponent<Image>().fillAmount = 0.33f * (GameController.instance.lives);

            if (GameController.instance.lives>0)
            {
                Destroy(collision.gameObject);
                collision.collider.enabled = false;
                playerAudio.PlayOneShot(crashSound, 1.0f);
                StartCoroutine(TurnonParticleCoroutine());
            }
            else
            {
                gameOver = true;
                Debug.Log("Game Over");
                playerAnim.SetBool("Death_b", true);
                GameController.instance.isStartRunning = false;
                dirtPart.Stop();
                score.text = ScoringSystem.theScore.ToString();
                scorePanel.SetActive(true);
            }

        }
    }

    IEnumerator TurnonParticleCoroutine()
    {
        dirtPart.Stop();
        yield return new WaitForSeconds(0.5f);
        dirtPart.Play();
    }
}
