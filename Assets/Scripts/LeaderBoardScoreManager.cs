﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using CloudOnce;
using GooglePlayGames;

public class LeaderBoardScoreManager : MonoBehaviour
{
    public Text highScore;
    public GameObject UserInfoPrefab;
    public GameObject Parent;
    public bool isRankingDisplayed;

    // Start is called before the first frame update
    void Start()
    {
        //Cloud.OnInitializeComplete += CloudOnceInitializeComplete;
        //Cloud.Initialize(false, true);

        isRankingDisplayed = false;

        if (ScoringSystem.GetLength() > 0)
        {
            CreateEntries();
        }
    }


    //public void CloudOnceInitializeComplete()
    //{
    //    Cloud.OnInitializeComplete -= CloudOnceInitializeComplete;
    //}

    private void CreateEntries()
    {

        for (int i=0;i<Mathf.Min(ScoringSystem.GetLength(), 10);i++)
        {
          
            if (ScoringSystem.names[i].Length > 0)
            {
                GameObject obj = Instantiate(UserInfoPrefab);
                obj.transform.SetParent(Parent.transform, false);
                obj.transform.localPosition = new Vector3(-40, 50 - (i * 40), 0);
                obj.GetComponent<UserInformationHandler>().Score.text = ScoringSystem.scores[i].ToString();
                obj.GetComponent<UserInformationHandler>().Name.text = ScoringSystem.names[i];
            }

        }
        isRankingDisplayed = true;

        CloudOnceManager.Instance.GetHighScore();
    }

    private void Update()
    {
        if (PlayGamesPlatform.Instance.IsAuthenticated() && !isRankingDisplayed)
        {
            CloudOnceManager.Instance.GetHighScore();
            if (ScoringSystem.GetLength() > 0)
            {
                CreateEntries();
            }
        }

        if(CloudOnceManager.Instance.reloadScores)
        {
            if (ScoringSystem.GetLength() > 0)
            {
                CreateEntries();
            }
            CloudOnceManager.Instance.reloadScores = false;
        }
    }

    //public void AddHighScore()
    //{
    //    highScore.text = PlayerPrefs.GetInt("Score", 0).ToString();
    //    Leaderboards.Leaderboard1.SubmitScore(PlayerPrefs.GetInt("Score", 0));
    //}

    //public void LoadScores()
    //{

    //    PlayGamesPlatform.Instance.LoadScores(,
    //             LeaderboardStart.PlayerCentered,
    //             1,
    //             LeaderboardCollection.Public,
    //             LeaderboardTimeSpan.AllTime,
    //         (LeaderboardScoreData data) => {
    //             Debug.Log(data.Valid);
    //             Debug.Log(data.Id);
    //             Debug.Log(data.PlayerScore);
    //             Debug.Log(data.PlayerScore.userID);
    //             Debug.Log(data.PlayerScore.formattedValue);
    //         });
    //}
}
