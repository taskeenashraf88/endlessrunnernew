﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Localization.Settings;

public class GameController : MonoBehaviour
{
    public static GameController instance;
    public bool isStartRunning = false;
    public GameObject displayText;
    public float spawnZ=0;

    public int lives = 3;
    // Use this for initialization
    void Start()
    {

       // ScoringSystem.theScore = 0;
        spawnZ = 0;
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {

            instance = this;
        }

        GetSetReady();

        

       

    }

    public void GetSetReady()
    {
        
        StartCoroutine(DisplayCoroutine());
    }

    IEnumerator DisplayCoroutine()
    {
        displayText.SetActive(true);
        if (!ScoringSystem.isSpanish)
        {
            displayText.GetComponent<Text>().text = "Ready...";

        }
        else
        {
            displayText.GetComponent<Text>().text = "Listo...";
        }

        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(2);

        if (!ScoringSystem.isSpanish)
        {

            displayText.GetComponent<Text>().text = "Go!!!";
        }
        else
        {
            displayText.GetComponent<Text>().text = "Vamos !!!";
        }

            yield return new WaitForSeconds(2);

        displayText.SetActive(false);
        isStartRunning = true;

    }
}
    



