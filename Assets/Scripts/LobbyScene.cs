﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using CloudOnce;
using GooglePlayGames;
using GooglePlayGames.BasicApi;


public class LobbyScene : MonoBehaviour
{
    public Text score;
    private string leaderboardId = "";
    string [] IDs;

    bool isScoreLoaded;
    //public LocalizedString myString="Player";


    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("lobby scene started");
        isScoreLoaded = false;
        CloudOnceManager.Instance.SetUpCloud();
        CloudOnceManager.Instance.AddHighScore();
       
 
            ScoringSystem.isSpanish= PlayerPrefs.GetInt("isSpanish", 0)==1;

    }
   

    void Update()
    {
        Debug.Log("lobby scene started");

        //if (!isScoreLoaded || CloudOnceManager.Instance.reloadLobbyScore)
        //{
        //    isScoreLoaded = true;
            //CloudOnceManager.Instance.reloadLobbyScore = false;

            Debug.Log("score loaded from leaderboard"+ CloudOnceManager.Instance.playerScore);
           

            if (CloudOnceManager.Instance.playerScore.Length> 0)
            {
                PlayerPrefs.SetInt("Score", Int32.Parse(CloudOnceManager.Instance.playerScore));
                Debug.Log("score loaded from leaderboard");

                if (Int32.Parse(CloudOnceManager.Instance.playerScore) < PlayerPrefs.GetInt("Score", 0))
                {
                    Debug.Log("Prefs score is higher");
                    CloudOnceManager.Instance.AddHighScore();
                    score.text = PlayerPrefs.GetInt("Score", 0).ToString();
                }
                else
                {
                    Debug.Log("leaderboard score is higher");

                    score.text = CloudOnceManager.Instance.playerScore;
                }
            }
            else
            {
                Debug.Log("score not loaded from leaderboard");
                Debug.Log(PlayerPrefs.GetInt("Score", 0));
                if (PlayerPrefs.GetInt("Score", 0) > ScoringSystem.theScore)
                {
                    Debug.Log("score loaded from prefs");


                    score.text = PlayerPrefs.GetInt("Score", 0).ToString();

                }
                else
                {
                    Debug.Log("score loaded from system");

                    score.text = ScoringSystem.theScore.ToString();
                }
        //    }

        }
    }

    
    //public void CloudOnceInitializeComplete()
    //{
    //    Cloud.OnInitializeComplete -= CloudOnceInitializeComplete;
    //    Debug.LogWarning("Initialized");
    //    AddHighScore();
    //    GetHighScore();
    //}

    //public void AddHighScore()
    //{
    //    //if (!PlayGamesPlatform.Instance.localUser.authenticated)
    //    //{
    //    //    Debug.Log("not logged in");
    //    //}
    //    //else
    //    //{
    //    Leaderboards.Leaderboard1.SubmitScore(PlayerPrefs.GetInt("Score", 0));

    //    //GetHighScore();
    //}

    //public void GetHighScore()
    //{
    //    int i = 0;
    //    leaderboardId = Leaderboards.Leaderboard1.ID;
    //    //PlayGamesPlatform.Instance.LoadScores(leaderboardId,
    //    //     LeaderboardStart.TopScores,
    //    //     10,
    //    //     LeaderboardCollection.Public,
    //    //     LeaderboardTimeSpan.AllTime,
    //    // (data) =>
    //    // {
    //    //     if (data.ApproximateCount > 0)
    //    //     {
    //    //         ScoringSystem.scores = new int[data.Scores.Length];
    //    //     ScoringSystem.names = new string[data.Scores.Length];

    //    //     Debug.Log(data.ApproximateCount);
    //    //     Debug.Log(data.Scores);

    //    //         foreach (var score in data.Scores)
    //    //         {
    //    //             ScoringSystem.scores[i] = Int32.Parse(score.formattedValue);
    //    //             ScoringSystem.names[i] = score.userID;
    //    //             Debug.Log(score.value + " - " + score.userID + " - " + score.leaderboardID);
    //    //         }
    //    //     }


    //    // });
    //    Debug.Log(leaderboardId);
    //    Debug.Log("PlayGamesPlatform.Instance)" + PlayGamesPlatform.Instance);
    //    Debug.Log("PlayGamesPlatform.Instance.IsAuthenticated()" + PlayGamesPlatform.Instance.localUser.authenticated);

    //    if (!PlayGamesPlatform.Instance.localUser.authenticated)
    //    {
    //        Debug.Log("not logged in");


    //    }
    //    else
    //    {
    //        IDs = new string[20];
    //        PlayGamesPlatform.Instance.LoadScores(
    //leaderboardId,
    //LeaderboardStart.TopScores,
    //20,
    //LeaderboardCollection.Public,
    //LeaderboardTimeSpan.AllTime,
    //(data) =>
    //{
    //    string mStatus = "Leaderboard data valid: " + data.Valid;
    //    mStatus += "\n approx:" + data.ApproximateCount + " have " + data.Scores.Length;
    //    Debug.Log(mStatus);

    //if (data.Scores.Length > 0)
    //{
    //    ScoringSystem.scores = new int[data.Scores.Length];
    //    ScoringSystem.names = new string[data.Scores.Length];

    //    Debug.Log(data.ApproximateCount);
    //    Debug.Log(data.Scores);

    //    foreach (var score in data.Scores)
    //    {
    //        IDs[i] = score.userID;
    //        ScoringSystem.scores[i] = Int32.Parse(score.formattedValue);
    //        Debug.Log(ScoringSystem.scores[i]);
    //            Debug.Log(score.userID);
    //            i++;
    //      }
    //        i = 0;
    //        Social.LoadUsers(IDs, (profile) =>
    //        {

    //            foreach (var p in profile)
    //            {
    //                ScoringSystem.names[i] = p.userName;
    //                Debug.Log(p.userName);
    //                Debug.Log(p.id);
    //                i++;

    //            }

    //        });



    //    }


    //});

    //        //PlayGamesPlatform.Instance.AskForLoadFriendsResolution((result) =>
    //        //{
    //        //    if (result == UIStatus.Valid)
    //        //    {

    //        //    }



    //        //    else
    //        //    {
    //        //        // User doesn’t agree to share the friends list.
    //        //    }
    //        //});
    //    }



    //    //if (ScoringSystem.scores == null)
    //    //{
    //    //    ScoringSystem.scores = new int[] { 20, 30 };
    //    //    ScoringSystem.names = new string[] { "test1", "test2" };

    //    //}


    //}


}


