﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;

public class LanguageController : MonoBehaviour
{

    IEnumerator Start()
    {
        // Wait for the localization system to initialize, loading Locales, preloading etc.
        yield return LocalizationSettings.InitializationOperation;
        if (PlayerPrefs.GetInt("isSpanish", 0) == 1)
        {
            LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[1];
        }
        else
        {
            LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[0];

        }
        //LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[0];
        //ScoringSystem.isSpanish = false;


    }

    IEnumerator ChangeLanguage(bool isSpanish)
    {
        // Wait for the localization system to initialize, loading Locales, preloading etc.
        yield return LocalizationSettings.InitializationOperation;
        if (isSpanish)
        {
            LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[1];
        }
        else
        {
            LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[0];

        }


    }

    public void onEnglishFlagTapped()
    {
        ScoringSystem.isSpanish = false;
        StartCoroutine(ChangeLanguage(false));
        PlayerPrefs.SetInt("isSpanish", 0);
        
    }

    public void onSpanishFlagTapped()
    {
        ScoringSystem.isSpanish = true;
        StartCoroutine(ChangeLanguage(true));
        PlayerPrefs.SetInt("isSpanish", 1);

    }


}
