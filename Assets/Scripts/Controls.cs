﻿using UnityEngine;
using System.Collections;

public class Controls : MonoBehaviour
{
    private GameObject player;
    private int min;
    private int sec;
    private float timecount;
    private float starttime;
    public int timeLimitseconds=10;
    public GameObject switchControlText;

    private bool invertControls;

    private void Start()
    {
        player = GameObject.Find("Player");
        starttime = Time.time;
        invertControls = false;
    }

    public void OnButtonTapped(bool isRightForJump)
    {
        if (!invertControls)
           player.GetComponent<PlayerMovement>().GetMovementControls(isRightForJump);
        else
            player.GetComponent<PlayerMovement>().GetMovementControls(!isRightForJump);

    }


    void Update()
    {
        timecount = Time.time - starttime;
        sec = (int)(timecount % 60f);
      //  Debug.Log(sec);
        if (sec>timeLimitseconds)
        {
            invertControls = !invertControls;
            //Debug.Log("inverting Controls");
            StartCoroutine(disableText());
            starttime = Time.time;
            

        }


    }

    IEnumerator disableText()
    {
        switchControlText.SetActive(true);
        yield return new WaitForSeconds(4);
        switchControlText.SetActive(false);

    }


}
