﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class LoadSceneController : MonoBehaviour
{

    public GameObject panel;
    public void load(string scene)
    {
        // If a popup is there close it first
        if (panel!=null)
        {
            panel.SetActive(false);
        }
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        SceneManager.LoadScene(scene);

    }


}
