﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using CloudOnce;

public class ScoringSystem : MonoBehaviour
{
    public GameObject scoreText;
    public static int theScore;

 

    public static bool isSpanish;

    public GameObject ScoringObject;

    //public int rankingDataLength = 10;
    public static int[] scores = new int[10];
    public static string[] names= new string[10];

    private void Start()
    {
        //-104.9
        Vector3 position = ScoringObject.transform.position;
        theScore = 0;
        //Debug.Log(Screen.width);
        if(!isSpanish)
        {
            ScoringObject.transform.position = new Vector3(Screen.width/-11.84f, position.y, position.z);
        }
        else
        {
            ScoringObject.transform.position = new Vector3(Screen.width/-90f, position.y, position.z);

        }


    }

    public static void ResetRankingData()
    {
        for (int i=0;i<10;i++)
        {
            scores[i] = 0;
            names[i] = "";
        }
    }

    private void Update()
    {
        scoreText.GetComponent<Text>().text = ": " + theScore;
        if (PlayerPrefs.GetInt("Score", 0) < theScore)
        {
            PlayerPrefs.SetInt("Score", theScore);
        }
        

    }

    public static int GetLength()
    {
        int length=0;
        for (int i = 0; i < 10; i++)
        {
            if (names[i].Length>0)
            {
                length++;
            }
            else
            {
                break;
            }
        }
        return length;
    }

    //public void CloudOnceInitializeComplete()
    //{
    //    Cloud.OnInitializeComplete -= CloudOnceInitializeComplete;
    //    Debug.LogWarning("Initialized");
    //}

    //public void AddHighScore()
    //{
    //    //highScore.text = PlayerPrefs.GetInt("Score", 0).ToString();
    //    Leaderboards.Leaderboard1.SubmitScore(PlayerPrefs.GetInt("Score", 0));
    //}



}
