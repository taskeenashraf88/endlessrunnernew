﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateObstacle : MonoBehaviour
{
   
    public float repeatRate = 0.75f;
    public float delaySpawn = 2;
    public float yPositionFloor = 31.43f;
    public float yPositionHigh = 33f;
    private float yPosition;
    public float lastZPosition = 0;

   
    public GameObject ObstaclePrefab;
    GameObject createdObject;
    PlayerMovement playerControllerScript;

    Vector3 playerPos;
    Vector3 obstaclePos;
   
    void Start()
    {
        playerPos = GameObject.Find("Player").transform.position;
        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerMovement>();

        InvokeRepeating("spawn", delaySpawn, repeatRate);


    }

    void spawn()
    {
        if (playerControllerScript.gameOver == false)
        {
            //Returns either 0,1 so check if going to create an obstacle or not
            if (Random.Range(0, 2) > 0.5f)
            {
                yPosition = yPositionFloor;
            }
            else
            {
                yPosition = yPositionHigh;

            }
            if (GameController.instance.spawnZ == 0)
                {
                    GameController.instance.spawnZ = playerPos.z+10;
                }
                //create an obstacle at random distance from the last saved position
                GameController.instance.spawnZ = GameController.instance.spawnZ + Random.Range(2, 8);
                obstaclePos = new Vector3(ObstaclePrefab.transform.position.x, yPosition, GameController.instance.spawnZ);
                createdObject = Instantiate(ObstaclePrefab, obstaclePos, ObstaclePrefab.transform.rotation);
                //createdObject.SetActive(false);
             }
        }
       

    

  

}
